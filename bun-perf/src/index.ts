import server from 'bunrest';
const app = server();

app.get('/', (req, res) => {
  res.send('hello world');
});

app.listen(8080, () => {
  console.log('App is listening on port 8080');
});
